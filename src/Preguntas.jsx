import React, { useEffect, useState } from 'react';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { Link } from 'react-router-dom';

const DraggableText = ({ text }) => {
  const [, drag] = useDrag({
    type: text,
    item: { value: text }, // Proporciona la información del elemento que se arrastra
  });

  return (
    <span ref={drag} className='cursor-move mx-1'>
      {text}
    </span>
  );
};

const Button = ({className, onClick, children}) => {
  return(
    <button onClick={onClick} className={'border border-black p-2 my-2 min-w-[80px] bg-slate-700 text-white '+ className}>{children}</button>
  );
};

const DropArea = ({setValid, valid, answers, options, name}) => {
  const [text, setText] = useState([]);
  const [count, setCount] = useState(0);

  useEffect(() => {
    let flag = true;
    text.map(
      (t) => {
        if(!answers.includes(t)){
          flag = false;
        }        
      }
    );
    if(flag && answers && count == answers.length){
      setValid({...valid, [name]: true});
    }
    else{
      setValid({...valid, [name]: false});
    }
  }, [text]);

  const [, drop] = useDrop({
    accept: options,
    drop: (droppedItem) => {
      if(!text.includes(droppedItem.value))
        setText([...text, droppedItem.value]);
        setCount(count + 1);
    },
  });

  return (
    <div className='flex flex-col'>
      <div className='border border-black h-min-20 min-h-[100px]' ref={drop}>
        {text.map((element)=>(<>{element}<br/></>))}
      </div>
      <div className='flex justify-end'><Button onClick={()=>{setText([]); setCount(0)}} className=''>Clean</Button></div>
    </div>
  );
};

export const Form = ({statement, options, tittle, ant, pos}) => {

  const [valid, setValid] = useState({});
  const [answers, setAnswers] = useState({});
  const [posibilities, setPosibilities] = useState([]);
  useEffect(
    () => {
      let tmp = [];
      let aux = {};
      statement.split("{o}").map(
        (element, index) => {
          if(options[index]){
            aux = ({...aux, [(options[index])[0]]: [...(aux[((options[index])[0])] || []), options[index][1]]});
            tmp = ([...tmp, (options[index])[1]]);
          }
        }
      );
      setAnswers(aux);
      setPosibilities(tmp);
    }, []
  )

  const handleButton = () => {
    let msg = "";

    console.log(valid)
    if(valid['C'] && valid['M'] && valid['A']) msg = "¡Bien hecho! :D";
    else msg = "¡Fail! D:";
    alert(msg);
  };

  return (
    <DndProvider backend={HTML5Backend}>
      <div className='p-10'>
        <div className='text-3xl text-center mb-10 font-bold'>{tittle}</div>
        <div className=''>
          {
            statement.split("{o}").map(
              (element, index) => {
                if(options[index])
                  return <>{element} <DraggableText text={(options[index])[1]}></DraggableText></>;
              }
            )
          }
        </div>
        <div className='flex flex-col space-y-5'>
          <div className='mt-5'>
            <h2>Clases</h2>
            <DropArea answers={answers.C} name={"C"} valid={valid} options={posibilities} setValid={setValid}/>
          </div>
          <div>
            <h2>Atributos</h2>
            <DropArea answers={answers.A} name={"A"} valid={valid} options={posibilities} setValid={setValid}/>
          </div>
          <div>
            <h2>Métodos</h2>
            <DropArea answers={answers.M} name={"M"} valid={valid} options={posibilities} setValid={setValid}/>
          </div>
          <div className='flex justify-center'><Button className={"bg-orange-600 rounded-md w-1/12"} onClick={handleButton}>Validar</Button></div>
          <div className='flex justify-center'>
              <Link to={ant} className='bg-slate-800 text-white p-3'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8"/>
              </svg></Link>
              <Link to={pos} className='bg-slate-800 text-white p-3'><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"/>
              </svg></Link>
          </div>
        </div>
      </div>
    </DndProvider>
  );
};
