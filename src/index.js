// src/index.js
import React from 'react';
import { createRoot } from 'react-dom/client';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { BrowserRouter as Router, Routes, Route, Form, Link, BrowserRouter } from 'react-router-dom';
import './index.css';
import './App.css';
import reportWebVitals from './reportWebVitals';
import App from './App';

const root = document.getElementById('root');
const reactRoot = createRoot(root);

reactRoot.render(
  
  <React.StrictMode>
    <BrowserRouter>
      <App></App>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
