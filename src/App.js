// src/App.jsx
import React from 'react';
import { BrowserRouter, Link, Route, Router, Routes } from 'react-router-dom';
import { Form } from './Preguntas';

const App = () => {
  return (
    <div>
      <Routes>
        <Route path='/' element={<div className="min-h-screen flex flex-col items-center justify-center bg-purple-950">
                <h1 className="text-5xl font-bold mb-6 bg-gradient-to-r from-red-500 via-yellow-500 to-green-500 bg-clip-text text-transparent border-b-2 border-black animate-rainbow-text">
                  Guess The Items
                </h1>
                <p className="text-white text-lg max-w-prose mb-8">
                  Bienvenido a Guess The Items, con esta herramienta{' '}
                  <strong className="font-bold">interactiva</strong> podrás relacionarte un poco más con la programación orientada a objetos, ya que por medio de historias, poco a poco irás aprendiendo a identificar cada uno de los elementos más importantes que componen los objetos (Clase, atributo y método).
                </p>
                <Link to="/preguntas/1">
                  <button className="bg-pink-500 text-white py-2 px-4 rounded-full hover:bg-white hover:text-pink-500 transition duration-300">
                    ¡¡COMENCEMOS!!
                  </button>
                </Link>
              </div>}>

        </Route>
        <Route path='/preguntas/1' element={
          <div>
              <Form statement={"Un banco ha decidido modernizar su sistema de gestión de {o}. Cada cuenta bancaria está identificada por un {o} único y tiene un {o}. Además, cada cuenta tiene un {o}. Los titulares pueden realizar {o} y {o} en sus cuentas. El banco necesita un sistema que permita a los titulares realizar estas operaciones de manera segura."} tittle={"Gestión de cuentas bancarias"} options={ [['C',"cuentas bancarias"],['A',"número de cuenta"],['A',"saldo"], ['A',"titular"], ['M',"depositos"], ['M',"retiros"]]} ant={"/"} pos={"/preguntas/2"}></Form>
          </div>
        }>
        </Route>
        <Route path='/preguntas/2' element={
          <div>
              <Form statement={"En una biblioteca digital, se desea implementar un sistema para gestionar libros electrónicos. Cada {o} tiene un {o}, un {o}, y una {o}. Los usuarios pueden {o} y {o} los libros. Además, el sistema debe llevar un registro de la {o} de cada libro."} tittle={"Gestión de Biblioteca Digital"} options={ [["C", "libro"], ["A", "título"], ["A", "autor"], ["A", "cantidad de páginas"], ["M", "prestar"], ["M", "devolver"], ["A", "disponibilidad"]]} ant={"/preguntas/1"} pos={"/preguntas/3"}></Form>
          </div>
        }>
        </Route>
        <Route path='/preguntas/3' element={
          <div>
              <Form statement={"En un juego de rol, los jugadores pueden elegir la {o} de un {o}, como guerrero, mago y arquero. Cada personaje tiene un {o}, {o} (HP), y {o} únicas. Durante el juego, los personajes pueden {o} y {o}."} tittle={"Simulación de un Juego de Rol (RPG)"} options={[["A", "clase"], ["C", "personaje"], ["A", "nivel"], ["A", "puntos de vida"], ["A", "habilidades"], ["M", "subir de nivel"], ["M", "adquirir nuevas habilidades"]]} ant={"/preguntas/2"} pos={"/preguntas/4"}></Form>
          </div>
        }>
        </Route>
        <Route path='/preguntas/4' element={
          <div>
              <Form statement={"En una empresa, se necesita un sistema para gestionar a los empleados. Cada {o} tiene un {o}, un {o}, y un {o}. Además, se desea {o} de las {o} y permitir {o}. Los empleados también pueden acceder a información como {o} y {o}."} tittle={"Sistema de Gestión de Empleados"} options={ [["C", "empleado"], ["A", "nombre"], ["A", "cargo"], ["A", "salario"], ["M", "llevar registro"], ["A", "horas trabajadas"], ["M", "ajustar el salario"], ["A", "días de vacaciones"], ["A", "beneficios"]]} ant={"/preguntas/3"} pos={"/preguntas/5"}></Form>
          </div>
        }>
        </Route>
        <Route path='/preguntas/5' element={
          <div>
              <Form statement={"En una agencia de viajes, se quiere implementar un sistema de reservas de vuelos. Cada {o} tiene un {o}, un {o}, una {o} y {o}, y una {o}. Los clientes pueden {o}, {o} y {o}."} tittle={"Sistema de Reservas de Vuelos"} options={ [ ["C", "vuelo"], ["A", "número de vuelo"], ["A", "destino"], ["A", "fecha de salida"], ["A", "hora de salida"], ["A", "lista de pasajeros"], ["M", "realizar reservas"], ["M", "cancelar reservas"], ["M", "consultar la información de los vuelos disponibles"] ]} ant={"/preguntas/4"} pos={"/"}></Form>
          </div>
        }>
        </Route>

      </Routes>
    </div>    
  );
}

export default App;
